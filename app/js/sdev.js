jQuery(document).ready(function($){
    $(document).foundation();

    // Login button listener here
    $('#loginButton').click(function(event){
        event.preventDefault();

        var params = {
            'login': $('#loginFormLogin').val(),
            'password': $('#loginFormPassword').val()
        };

        $.ajax({
            url: '../api/index.php/api/users/auth/',
            type: 'POST',
            data: JSON.stringify(params),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
            success: function(data) {
                var error = data.errorCode;

                if (error == 1){
                    $('#loginFormError').html('Error: ' + data.content);
                    $('#loginFormError').show();
                    $('#loginFormLogin').focus();
                } else if (error == 0) {
                    data = data.content;

                    var login = data.login;
                    var firstName = data.first_name;
                    var lastName = data.last_name;
                    var phoneNumber = data.phone_number;

                    $('#login').html(login);
                    $('#firstName').html(firstName);
                    $('#lastName').html(lastName);
                    $('#phoneNumber').html(phoneNumber);

                    $('#isNotAuth').hide();
                    $('#greeting').hide();
                    $('#isAuth').show();
                    $('#info').show();
                    $('#successfullyLoggedOut').hide();
                    $('#successfullyLoggedOutMessage').html('');
                    $('#successfullyLoggedIn').show();
                    $('#successfullyLoggedInMessage').html('You are successfully logged in');

                    $('#loginPopup').foundation('reveal', 'close');
                    clearLoginForm();
                }
            }
        });
    });

    // Register button listener here
    $('#registerButton').click(function(event){
        event.preventDefault();

        var params = {
            'login': $('#registerFormLogin').val(),
            'password': $('#registerFormPassword').val(),
            'passwordConfirmation': $('#registerFormPasswordConfirmation').val(),
            'firstName': $('#registerFormFirstName').val(),
            'lastName': $('#registerFormLastName').val(),
            'phoneNumber': $('#registerFormPhoneNumber').val()
        };

        $.ajax({
            url: '../api/index.php/api/users/',
            type: 'POST',
            data: JSON.stringify(params),
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
            success: function(data) {
                var error = data.errorCode;

                if (error == 1){
                    $('#registerFormError').html('Error: ' + data.content);
                    $('#registerFormError').show();
                    $('#registerFormLogin').focus();
                } else if (error == 0) {
                    data = data.content;
//                    console.log(data);
                    $('#successfullyLoggedOut').hide();
                    $('#successfullyLoggedOutMessage').html('');
                    $('#successfullyLoggedIn').show();
                    $('#successfullyLoggedInMessage').html(data);

                    $('#loginPopup').foundation('reveal', 'close');
                    clearRegisterForm();
                }
            }
        });
    });

    // Logout
    $('#logout').click(function(event){
        event.preventDefault();

        $.ajax({
            url: '../api/index.php/api/users/logout/',
            type: 'POST',
            dataType: 'json',
            async: false,
            success: function(data) {
                //console.log(data);
                var error = data.errorCode;

                if (error == 1){
                    $('#isNotAuth').hide();
                    $('#greeting').hide();
                    $('#isAuth').show();
                    $('#info').show();
                } else if (error == 0) {
                    data = data.content;

                    $('#isNotAuth').show();
                    $('#greeting').show();
                    $('#isAuth').hide();
                    $('#info').hide();
                    $('#successfullyLoggedOut').show();
                    $('#successfullyLoggedOutMessage').html(data);
                    $('#successfullyLoggedIn').hide();
                    $('#successfullyLoggedInMessage').html('');
                }
            }
        });
    });

    // Initial check
    $.ajax({
        url: '../api/index.php/api/users/check/',
        type: 'POST',
        async: false,
        dataType: 'json',
        success: function(data) {
            var error = data.errorCode;

            if (error == 1){
                $('#isNotAuth').show();
                $('#greeting').show();
                $('#isAuth').hide();
                $('#info').hide();
            }else{
                data = data.content;

                var login = data.login;
                var firstName = data.first_name;
                var lastName = data.last_name;
                var phoneNumber = data.phone_number;

                $('#login').html(login);
                $('#firstName').html(firstName);
                $('#lastName').html(lastName);
                $('#phoneNumber').html(phoneNumber);

                $('#isNotAuth').hide();
                $('#greeting').hide();
                $('#isAuth').show();
                $('#info').show();
            }
        }
    });
});

function clearLoginForm(){
    $('#loginFormLogin').val("");
    $('#loginFormPassword').val("");
    $('#loginFormError').html('');
    $('#loginFormError').hide();
}

function clearRegisterForm(){
    $('#registerFormLogin').val("");
    $('#registerFormPassword').val("")
    $('#registerFormPasswordConfirmation').val("");
    $('#registerFormFirstName').val("");
    $('#registerFormLastName').val("");
    $('#registerFormPhoneNumber').val("");

    $('#registerFormError').html('');
    $('#registerFormError').hide();
}