<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Mercury | Welcome</title>
    <link rel="stylesheet" href="css/foundation.css" />
    <link rel="stylesheet" href="css/style.css" />
    <script src="js/vendor/modernizr.js"></script>
  </head>
  <body>
    <div class="row header">
      <div class="large-12 columns">
          <div class="large-8 columns logo">
            <img src="./img/logo.png" />
          </div>
          <div id="isNotAuth" class="large-4 columns menu text-right">
              <a href="#" data-reveal-id="loginPopup" data-reveal>login</a> | <a href="#" data-reveal-id="registerPopup" data-reveal>register</a>
          </div>
          <div id="isAuth" class="large-4 columns menu text-right">
              <a href="#" id="logout" data-reveal>logout</a>
          </div>
      </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <div style="display: none;" id="successfullyLoggedIn" data-alert class="alert-box success">
                <span id="successfullyLoggedInMessage"></span>
                <a href="#" class="close">&times;</a>
            </div>
            <div style="display: none;" id="successfullyLoggedOut" data-alert class="alert-box warning">
                <span id="successfullyLoggedOutMessage"></span>
                <a href="#" class="close">&times;</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <div class="panel">
                <div id="greeting">
                    <h4>Welcome to web system</h4>
                    This is MercuryDevelopment test task for web-developer position
                </div>
                <div id="info">
                    <h4>Profile info</h4>
                    <b>Login: </b> <span id="login"></span><br/>
                    <b>First Name: </b> <span id="firstName"></span><br/>
                    <b>Last Name: </b> <span id="lastName"></span><br/>
                    <b>Phone Number: </b> <span id="phoneNumber"></span><br/>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="large-12 columns">
            <hr/>
            2014 &copy; Anton Shabanov
        </div>
    </div>

    <div id="loginPopup" class="reveal-modal" data-reveal>

            <fieldset>
                <legend><h2>Login form</h2></legend>

                <small style="display: none;" id="loginFormError" class="error"></small>
                <label>Your login
                    <input type="text" id="loginFormLogin" placeholder="Please, type your login here...">
                </label>

                <label>Your password
                    <input type="password" id="loginFormPassword" placeholder="Please, type your password here...">
                </label>

                <a href="#" id="loginButton" class="button right">Enter the system</a>
            </fieldset>

	  <a class="close-reveal-modal">&#215;</a>
	</div>
    <div id="registerPopup" class="reveal-modal" data-reveal>
        <fieldset>
            <legend><h2>Registration form</h2></legend>

            <small style="display: none;" id="registerFormError" class="error"></small>
            <label>Your login
                <input type="text" id="registerFormLogin" placeholder="Please, type your login here...">
            </label>

            <label>Your password
                <input type="password" id="registerFormPassword" placeholder="Please, type your password here...">
            </label>

            <label>Your password confirmation
                <input type="password" id="registerFormPasswordConfirmation" placeholder="Please, retype your password here...">
            </label>

            <label>Your first name
                <input type="text" id="registerFormFirstName" placeholder="Please, type your first name here...">
            </label>

            <label>Your last name
                <input type="text" id="registerFormLastName" placeholder="Please, type your last name here...">
            </label>

            <label>Your phone number
                <input type="text" id="registerFormPhoneNumber" placeholder="Please, type your phone number here...">
            </label>

            <a href="#" id="registerButton" class="button right">Register me now</a>
        </fieldset>
        <a class="close-reveal-modal">&#215;</a>
    </div>
    
    <script src="js/vendor/jquery.js"></script>
    <script src="js/foundation.min.js"></script>
    <script src="js/sdev.js"></script>
  </body>
</html>
