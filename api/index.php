<?php

require_once __DIR__.'/../config/config.php';
require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/../orm/rb.php';
require_once __DIR__.'/../logic/helper.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

R::setup(DB_DRIVER.':host='.DB_HOST.';dbname='.DB_NAME, DB_USER, DB_PASSWORD);

$app = new Silex\Application();

// Session registration in Silex micro-framework
$app->register(new Silex\Provider\SessionServiceProvider());

// Gets all users
$app->get('/api/users/', function() {
    $result = array();

    foreach (Helper::getAllUsers() as $rbUser){
        $result[] = Helper::convertRBUserToArray($rbUser);
    }

    return json_encode(array(
        'errorCode' => '0',
        'content' => $result
    ));
});

// Creates a user in database
$app->post('/api/users/', function(Request $request){
    try{
        if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
            $rbUser = json_decode($request->getContent());

            Helper::userFieldValidation($rbUser);

            Helper::createUser(array(
                'login' => $rbUser->login,
                'password' => $rbUser->password,
                'firstName' => $rbUser->firstName,
                'lastName' => $rbUser->lastName,
                'phoneNumber' => $rbUser->phoneNumber
            ));

            Helper::sendSimpleMail($rbUser->login);

            return new Response(json_encode(array(
                'errorCode' => '0',
                'content' => 'User has been created successfully'
            )), 200);

        }else{
            throw new Exception('JSON expected..');
        }
    } catch(Exception $ex){
        return new Response(json_encode(array(
            'errorCode' => '1',
            'content' => $ex->getMessage()
        )), 200);
    }
});

// Deletes all users from database
$app->delete('/api/users/', function(){
   try{
       Helper::deleteAllUsers();

       return new Response(json_encode(array(
           'errorCode' => '0',
           'content' => 'Users have been deleted successfully'
       )), 200);
   }catch (Exception $ex){
       return new Response(json_encode(array(
           'errorCode' => '1',
           'content' => $ex->getMessage()
       )), 200);
   }
});

// User auth
$app->post('/api/users/auth/', function(Request $request) use ($app) {
   try{
       if (0 === strpos($request->headers->get('Content-Type'), 'application/json')) {
           $rbUser = json_decode($request->getContent());

           $userData = Helper::isUserCorrect($rbUser);

           if ($userData){
               $user = reset($userData);

               $app['session']->set('userId', $user['id']);

               return json_encode(array(
                   'errorCode' => '0',
                   'content' => ($user)
               ));
           }else{
               throw new Exception('No such user found..');
           }
       }else{
           throw new Exception('JSON expected..');
       }

   }catch (Exception $ex){
       return new Response(json_encode(array(
           'errorCode' => '1',
           'content' => $ex->getMessage()
       )), 200);
   }
});

// Initial check for user session and so on
$app->post('/api/users/check/', function(Request $request) use ($app) {
    try{
        session_start();
        if ($app['session']->get('userId')){
            $user = Helper::getUserById($app['session']->get('userId'));

            return json_encode(array(
                'errorCode' => 0,
                'content' => Helper::convertRBUserToArray($user)
            ));
        }else{
            throw new Exception('User is not authorized..');
        }
    }catch (Exception $ex){
        return new Response(json_encode(array(
            'errorCode' => '1',
            'content' => $ex->getMessage()
        )), 200);
    }
});

// Logout
$app->post('/api/users/logout/', function(Request $request) use ($app){
    try{
        $request->getSession()->clear();

        return json_encode(array(
            'errorCode' => '0',
            'content' => 'User has been successfully logged out'
        ));
        #$app['session']->set('userId', null);
    }catch (Exception $ex){
        return new Response(json_encode(array(
            'errorCode' => '1',
            'content' => $ex->getMessage()
        )), 200);
    }
});

$app->put('/api/users/', function (Request $request) use ($app) {
    return new Response(json_encode(array(
        'errorCode' => '1',
        'content' => 'No realisation found in MercuryWebSystem for PUT method for collection. Thank you for testing ;)'
    )), 404);
});

$app->post('/api/users/{userId}', function ($userId) use ($app) {
    return new Response(json_encode(array(
        'errorCode' => '1',
        'content' => 'No such method found in REST-api. Thank you for testing ;)'
    )), 404);
});

$app->get('/api/users/{userId}', function ($userId) use ($app) {
    return new Response(json_encode(array(
        'errorCode' => '1',
        'content' => 'No realisation found in MercuryWebSystem for GET method for single user. Thank you for testing ;)'
    )), 404);
});

$app->put('/api/users/{userId}', function ($userId) use ($app) {
    return new Response(json_encode(array(
        'errorCode' => '1',
        'content' => 'No realisation found in MercuryWebSystem for PUT method for single user. Thank you for testing ;)'
    )), 404);
});

$app->delete('/api/users/{userId}', function ($userId) use ($app) {
    return new Response(json_encode(array(
        'errorCode' => '1',
        'content' => 'No realisation found in MercuryWebSystem for DELETE method for single user. Thank you for testing ;)'
    )), 404);
});

$app->run();