<?php

class Helper {
    public static function createUser($params){
        try{
            $user = R::dispense('users');

            $user->login = strtolower($params['login']);
            $user->password = md5(md5($params['password']));
            $user->first_name = $params['firstName'];
            $user->last_name = $params['lastName'];
            $user->phone_number = $params['phoneNumber'];

            R::store($user);
            return $user;
        } catch (Exception $ex) {
            throw $ex;
        }

    }

    public static function getUserById($id){
        try{
            return R::load('users', $id);
        }catch (Exception $ex){
            throw $ex;
        }
    }

    public static function deleteAllUsers(){
        try{
            R::wipe('users');
        }catch(Exception $ex){
            throw $ex;
        }
    }

    public static function getAllUsers(){
        try{
            return R::findAll('users');
        }catch(Exception $ex){
            throw $ex;
        }
    }

    public static function convertRBUserToArray($rbUser){
        try{
            return array(
                'id' => $rbUser->id,
                'login' => $rbUser->login,
                'password' => $rbUser->password,
                'first_name' => $rbUser->first_name,
                'last_name' => $rbUser->last_name,
                'phone_number' => $rbUser->phone_number
            );
        }catch (Exception $ex){
            throw $ex;
        }
    }

    public static function isUserCorrect($rbUser){
        try{
            return R::getAll('select * from users where login = :login and password = :password',array(
                ':login' => $rbUser->login,
                ':password' => md5(md5($rbUser->password))
            ));
        }catch (Exception $ex){
            throw $ex;
        }
    }

    public static function isUserExists($login){
        try{
            return R::getAll('select * from users where login = :login',array(
                ':login' => $login,
            ));
        }catch (Exception $ex){
            throw $ex;
        }
    }

    public static function userFieldValidation($rbUser){
        try{
            if (empty($rbUser->login))
                throw new Exception('Login can`t be blank..');

            if (!filter_var($rbUser->login, FILTER_VALIDATE_EMAIL))
                throw new Exception('Login validation error. Try to type correct email..');

            if (self::isUserExists($rbUser->login))
                throw new Exception('Login has already existed. Try to type new one..');

            if (empty($rbUser->password))
                throw new Exception('Password can`t be blank..');

            if ($rbUser->password !== $rbUser->passwordConfirmation)
                throw new Exception('Please check your password confirmation..');

            if (empty($rbUser->firstName))
                throw new Exception('First name can`t be blank..');

            if (empty($rbUser->lastName))
                throw new Exception('Last name can`t be blank..');

            if (empty($rbUser->phoneNumber))
                throw new Exception('Phone number can`t be blank..');

            if (!preg_match("/^[0-9]{11}$/", $rbUser->phoneNumber))
                throw new Exception('Phone number should be like this 89123456789..');

        }catch (Exception $ex){
            throw $ex;
        }
    }

    public static function sendSimpleMail($email, $theme = 'MercuryWeb', $message = 'Welcome to MercuryWebSystem. Glad to see you with us ;)'){
        // Simple and stupid mail realisation - just for fun
        try{
            mail($email, $theme, $message);
        }catch (Exception $ex){
            throw $ex;
        }
    }
} 